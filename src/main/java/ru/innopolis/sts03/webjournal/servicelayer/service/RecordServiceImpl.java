package ru.innopolis.sts03.webjournal.servicelayer.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Service;

import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.innopolis.sts03.webjournal.hibernate.entity.RecordEntity;
import ru.innopolis.sts03.webjournal.hibernate.entity.UserEntity;
import ru.innopolis.sts03.webjournal.model.Record;
import ru.innopolis.sts03.webjournal.model.User;
import ru.innopolis.sts03.webjournal.repository.RecordEntityRepository;
import ru.innopolis.sts03.webjournal.servicelayer.exception.ExceptionService;

/**
 * Сервис работы с записями пользователей
 */
@Service
public class RecordServiceImpl implements RecordService {

    private static final Logger LOG = LoggerFactory.getLogger(RecordServiceImpl.class);

    @Autowired
    private UserService userService;

    @Autowired
    RecordEntityRepository recordEntityRepository;


    /**
     * @return все записи всех пользователей
     * */
    public List<Record> getAll() {
        List<RecordEntity> recordEntities = recordEntityRepository.findAll();
        List<Record> records = new ArrayList<>();

        for(RecordEntity re: recordEntities) {
            records.add(re.convertToPOJO());
        }

        return records;
    }


    /**
     * @param user - пользователь
     * @return все записи одного пользователей
     * */
    public List<Record> getAllByUser(User user) {
        UserEntity userEntity = new UserEntity();
        userEntity.makeFromPOJO(user);
        List<RecordEntity> recordEntities = recordEntityRepository.findByUser(userEntity);

        List<Record> records = new ArrayList<>();

        for(RecordEntity re: recordEntities) {
            records.add(re.convertToPOJO());
        }
        return records;
    }



    /**
     * @return true - если добавление записи успешно
     * */
    @Secured("ROLE_USER")
    public boolean addRecord(String login, String title, String content) {
        User user = userService.getUserByLogin(login);
        UserEntity userEntity = new UserEntity();
        userEntity.convertToPOJO();

        if ( userEntity != null && title != null && content != null && !title.trim().equals("") && !content.trim().equals("") ) {
            //set
            RecordEntity record = new RecordEntity();
            record.setUser(userEntity);
            record.setTitle(title);
            record.setContent(content);
            record.setCreatedate(new Timestamp(System.currentTimeMillis()));

            recordEntityRepository.save(record);
            return true;
        }
        return false;
    }



    /**
     * @return true - если редактирование записи успешно
     * */
    @Secured("ROLE_USER")
    public boolean editRecord(Record record) throws ExceptionService {
        RecordEntity recordEntity = new RecordEntity();
        recordEntity.makeFromPOJO(record);

        User user = userService.getUserById(record.getId());
        UserEntity userEntity = new UserEntity();
        userEntity.makeFromPOJO(user);

        recordEntity.setUser(userEntity);

        if ( record.getTitle() != null && record.getContent() != null && !record.getTitle().trim().equals("") && !record.getContent().trim().equals("") ) {
            recordEntityRepository.saveAndFlush(recordEntity);
            return true;
        } else {
            return false;
        }
    }



    /**
     * @return RecordEntityEntity - получаем запись пользователя по её id
     * */
    public Record getElementById(int recordId) {
        RecordEntity recordEntity = recordEntityRepository.findById(recordId);
        Record record = recordEntity.convertToPOJO();
        return record;
    }


    /**
     * @return true - если удаление успешно
     * */
    @Secured("ROLE_USER")
    public boolean deleteRecord(int recordId) throws ExceptionService {
        //if проверка на пользователя
        recordEntityRepository.delete(recordId);
        return true;
    }
}
