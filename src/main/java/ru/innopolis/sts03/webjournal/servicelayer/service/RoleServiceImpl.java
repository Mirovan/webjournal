package ru.innopolis.sts03.webjournal.servicelayer.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import java.sql.SQLException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.innopolis.sts03.webjournal.hibernate.entity.RoleEntity;
import ru.innopolis.sts03.webjournal.repository.RoleEntityRepository;

/**
 * Created by mmm on 08.01.2017.
 */
@Service
public class RoleServiceImpl {

    private static final Logger LOG = LoggerFactory.getLogger(RoleServiceImpl.class);

    @Autowired
    RoleEntityRepository roleEntityRepository;

    public RoleEntity getElementByName(String name) throws SQLException {
        return roleEntityRepository.findByName(name);
    }

}