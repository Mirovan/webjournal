package ru.innopolis.sts03.webjournal.servicelayer.service;

import ru.innopolis.sts03.webjournal.hibernate.entity.RecordEntity;
import ru.innopolis.sts03.webjournal.hibernate.entity.UserEntity;
import ru.innopolis.sts03.webjournal.model.Record;
import ru.innopolis.sts03.webjournal.model.User;
import ru.innopolis.sts03.webjournal.servicelayer.exception.ExceptionService;

import java.util.List;

public interface RecordService {

    List<Record> getAll();

    List<Record> getAllByUser(User user);

    boolean addRecord(String login, String title, String content);

    boolean editRecord(Record record) throws ExceptionService;

    Record getElementById(int recordId);

    boolean deleteRecord(int recordId) throws ExceptionService;

}
