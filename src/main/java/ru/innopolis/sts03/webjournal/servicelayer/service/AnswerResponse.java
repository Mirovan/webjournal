package ru.innopolis.sts03.webjournal.servicelayer.service;

/**
 * Класс возвращающий ошибку в бизнес логике (дубликат email, login и т.д.)
 */
public class AnswerResponse {
    private boolean error;
    private String errorMsg;

    public AnswerResponse(){
        this.error = false;
    }

    public AnswerResponse(String error) {
        this.error = true;
        this.errorMsg = error;
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }
}
