package ru.innopolis.sts03.webjournal.servicelayer.service;

import ru.innopolis.sts03.webjournal.hibernate.entity.UserEntity;
import ru.innopolis.sts03.webjournal.model.User;
import ru.innopolis.sts03.webjournal.servicelayer.exception.ExceptionService;

import java.util.List;

/**
 * Created by mmm on 11.01.2017.
 */
public interface UserService {
    List<User> getAll();

    AnswerResponse registerUser(User user);

    User getUserByLogin(String login);

    User getUserById(int userid) throws ExceptionService;

    String getPrincipal();
}
