package ru.innopolis.sts03.webjournal.servicelayer.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;


import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.innopolis.sts03.webjournal.hibernate.entity.RoleEntity;
import ru.innopolis.sts03.webjournal.hibernate.entity.UserEntity;
import ru.innopolis.sts03.webjournal.model.User;
import ru.innopolis.sts03.webjournal.repository.RoleEntityRepository;
import ru.innopolis.sts03.webjournal.repository.UserEntityRepository;
import ru.innopolis.sts03.webjournal.servicelayer.exception.ExceptionService;

/**
 * Сервис - раюоты с аккаунтом пользователя
 */
@Service
public class UserServiceImpl implements UserService {

    private static final Logger LOG = LoggerFactory.getLogger(UserServiceImpl.class);


    @Autowired
    UserEntityRepository userEntityRepository;

    @Autowired
    RoleEntityRepository roleEntityRepository;


    /**
     * @return получить всех польователей
     * */
    public List<User> getAll() {
        List<UserEntity> userEntities = userEntityRepository.findAll();
        List<User> users = new ArrayList<>();

        for(UserEntity ue: userEntities) {
            users.add(ue.convertToPOJO());
        }

        return users;
    }

    @Override
    public AnswerResponse registerUser(User user) {
        UserEntity userEntity = new UserEntity();
        userEntity.makeFromPOJO(user);
        RoleEntity roleEntity = roleEntityRepository.findByName("ROLE_USER");
        userEntity.setRoleEntity(roleEntity);
        userEntity.setEnabled(1);

        //проверям - все ли поля заполнены
        if ( userEntity.getLogin().equals("") ||
            userEntity.getPassword().equals("") ||
            userEntity.getName().equals("") ||
                userEntity.getSirname().equals("")) {

            return new AnswerResponse("Заполните все поля");
        } else {

            //Проверяем - занят ли логин
            if (userEntityRepository.findByLogin(userEntity.getLogin()) == null) { //логин свободен
                //проверяем email на уникальность и корректность
                if (userEntityRepository.findByEmail(userEntity.getEmail()) != null) {
                    return new AnswerResponse("Email занят");
                } else {
                    userEntityRepository.save(userEntity);
                }
            } else {
                //логин занят
                return new AnswerResponse("Логин занят");
            }
        }

        return new AnswerResponse();
    }


    @Override
    public User getUserByLogin(String login) {
        UserEntity userEntity = userEntityRepository.findByLogin(login);
        User user = userEntity.convertToPOJO();
        return user;
    }

    @Override
    public User getUserById(int userid) throws ExceptionService {
        UserEntity userEntity = userEntityRepository.findById(userid);
        User user = userEntity.convertToPOJO();
        return user;
    }

    @Override
    public String getPrincipal() {
        String userName = null;
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        if (principal instanceof UserDetails) {
            userName = ((UserDetails)principal).getUsername();
        } else {
            userName = principal.toString();
        }
        return userName;
    }

/*

    *//**
     * @return true - если регистрация успешна
     * *//*
    public AnswerResponse registerUser(UserEntity user) {

        //проверям - все ли поля заполнены
        if ( user.getLogin().equals("") ||
             user.getPassword().equals("") ||
             user.getName().equals("") ||
             user.getSirname().equals("")) {

            return new AnswerResponse("Заполните все поля");
        } else {

            //Проверяем - занят ли логин
            if (userDAO.getUserByLogin(user.getLogin()) == null) { //логин свободен
                //проверяем email на уникальность и корректность
                if (userDAO.getUserByEmail(user.getEmail()) != null) {
                    return new AnswerResponse("Email занят");
                } else {

                    user.setEnabled(1);

                    try {
                        RoleEntity role = roleDAO.getElementByName("ROLE_USER");
                        user.setRole(role);

                        userDAO.addElement(user);
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }


                }
            } else {
                //логин занят
                return new AnswerResponse("Логин занят");
            }
        }

        return new AnswerResponse();
    }


    *//**
     * @return true - если авторизация успешна
     * *//*
    public boolean authUser(String login, String password) {
        //получаем пользователя по логину паролю
        UserEntity user = userDAO.getUserByAuth(login, password);
        if ( user != null ) { //авторизация успешна
            return true;
        } else { //авторизация не удалась
            return false;
        }
    }

    *//**
     * @return UserEntity - пользователь по его login
     * *//*
    public UserEntity getUserByLogin(String login) {
        return userDAO.getUserByLogin(login);
    }

    *//**
     * @return true - если удаление успешно
     * *//*
    public UserEntity getUserById(int userid) throws ExceptionService {
        UserEntity user = null;
        try {
            user = userDAO.getElementByID(userid);
        } catch (SQLException e) {
            throw new ExceptionService();
        }
        return user;
    }


    *//**
     * Возвращает имя пользователя из Spring Security
     * *//*
    public String getPrincipal() {
        String userName = null;
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        if (principal instanceof UserDetails) {
            userName = ((UserDetails)principal).getUsername();
        } else {
            userName = principal.toString();
        }
        return userName;
    }*/

}
