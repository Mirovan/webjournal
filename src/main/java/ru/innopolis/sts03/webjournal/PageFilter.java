package ru.innopolis.sts03.webjournal;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;


/**
 * Фильтр - определяем доступ к страницам и производит перекодировку в UTF-8
 */
public class PageFilter implements Filter {

    private String encoding = "utf-8";


    /**
     * Определяет доступ к страницам в зависимости от го зарегистрирован и пользователь
     * */
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChain) throws IOException, ServletException {
        request.setCharacterEncoding(encoding);

        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse resp = (HttpServletResponse) response;
        String url = req.getRequestURI();

        //если Не авторизован
        if ( req.getSession().getAttribute("login") == null &&
             url.contains("/user/")
           ) {
            resp.sendRedirect("/auth/");
            return;
        } else if ( req.getSession().getAttribute("login") != null &&
                (url.equals("/register/") || url.equals("/auth/"))
                ) { //если авторизован
            resp.sendRedirect("/user/");
            return;
        }

        filterChain.doFilter(request, response);
    }


    /**
     * Перекодировка запросов в UTF-8, т.к. Томкат пишет абракадабру
     * */
    public void init(FilterConfig filterConfig) throws ServletException {
        String encodingParam = filterConfig.getInitParameter("encoding");
        if (encodingParam != null) {
            encoding = encodingParam;
        }
    }

    public void destroy() {
        // nothing todo
    }

}