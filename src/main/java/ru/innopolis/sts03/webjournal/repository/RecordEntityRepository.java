package ru.innopolis.sts03.webjournal.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.innopolis.sts03.webjournal.hibernate.entity.RecordEntity;
import ru.innopolis.sts03.webjournal.hibernate.entity.UserEntity;

import java.util.List;

@Repository
public interface RecordEntityRepository extends JpaRepository<RecordEntity, Integer> {
    RecordEntity findById(int id);
    List<RecordEntity> findByUser(UserEntity user);
}
