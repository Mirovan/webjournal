package ru.innopolis.sts03.webjournal.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import ru.innopolis.sts03.webjournal.servicelayer.service.UserService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.sql.SQLException;

/**
 * Created by mmm on 08.01.2017.
 */

@Controller
public class AuthController {

    private static final Logger LOG = LoggerFactory.getLogger(AuthController.class);

    @Autowired
    private UserService userService;

    /**
     * Вывод Формы авторизации
     * */
    @RequestMapping(value = "/auth/", method = RequestMethod.GET)
    public ModelAndView authForm(HttpServletRequest req, HttpServletResponse resp) throws SQLException {
        ModelAndView mav = new ModelAndView();

        if ( req.getParameter("error") != null ) {
            mav.addObject("error", "Ошибка при авторизации");
        }

        mav.setViewName("auth/index");
        return mav;
    }


    /**
     * Выход пользователя
     * */
    @RequestMapping(value = "/auth/logout", method = RequestMethod.GET)
    public ModelAndView logout(HttpServletRequest req) throws SQLException {
        HttpSession session = req.getSession();
        session.invalidate();
        return new ModelAndView("redirect:/auth/");
    }

}
