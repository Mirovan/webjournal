package ru.innopolis.sts03.webjournal.controller;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import ru.innopolis.sts03.webjournal.model.Record;
import ru.innopolis.sts03.webjournal.servicelayer.service.RecordService;

import javax.servlet.http.*;
import java.sql.*;
import java.util.List;

/**
 * главная страница сайта
 * выводим последние записи всех пользователей
 */

@Controller
public class MainController {

    private static final Logger LOG = LoggerFactory.getLogger(MainController.class);

    @Autowired
    private RecordService recordService;

    /**
     * Отображение главной страницы
     * */
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public ModelAndView main(HttpServletRequest req, HttpServletResponse resp) throws SQLException {
        ModelAndView mav = new ModelAndView();

        List<Record> data = recordService.getAll();
        mav.addObject("data", data);
        mav.setViewName("main");
        return mav;
    }

}
