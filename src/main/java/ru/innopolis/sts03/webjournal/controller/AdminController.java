package ru.innopolis.sts03.webjournal.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import ru.innopolis.sts03.webjournal.hibernate.entity.UserEntity;
import ru.innopolis.sts03.webjournal.model.User;
import ru.innopolis.sts03.webjournal.servicelayer.service.UserService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.SQLException;


@Controller
public class AdminController {

    private static final Logger LOG = LoggerFactory.getLogger(AdminController.class);

    @Autowired
    private UserService userService;

    /**
     * Страница админа
     * */
    @RequestMapping(value = "/admin/", method = RequestMethod.GET)
    public ModelAndView authAdminForm(HttpServletRequest req, HttpServletResponse resp) throws SQLException {
        ModelAndView mav = new ModelAndView();

        String username = userService.getPrincipal();
        User user = userService.getUserByLogin(username);

        mav.addObject("user", user);

        mav.setViewName("admin/index");
        return mav;
    }


    @RequestMapping(value = "/403/", method = RequestMethod.GET)
    public ModelAndView error403(HttpServletRequest req, HttpServletResponse resp) throws SQLException {
        ModelAndView mav = new ModelAndView();

        mav.setViewName("403");
        return mav;
    }

}
