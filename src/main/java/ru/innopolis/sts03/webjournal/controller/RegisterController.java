package ru.innopolis.sts03.webjournal.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import ru.innopolis.sts03.webjournal.hibernate.entity.UserEntity;
import ru.innopolis.sts03.webjournal.model.User;
import ru.innopolis.sts03.webjournal.servicelayer.service.AnswerResponse;
import ru.innopolis.sts03.webjournal.servicelayer.service.UserService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.SQLException;

/**
 * Контроллер регистрации
 */

@Controller
public class RegisterController {

    private static final Logger LOG = LoggerFactory.getLogger(RegisterController.class);

    @Autowired
    private UserService userService;


    /**
     * форма регистрации
     * */
    @RequestMapping(value = "/register/", method = RequestMethod.GET)
    public ModelAndView registerForm(HttpServletRequest req, HttpServletResponse resp) throws SQLException {
        ModelAndView mav = new ModelAndView();

        mav.setViewName("register/index");
        return mav;
    }


    /**
     * Отправка POST формы регистрации
     * */
    @RequestMapping(value = "/register/", method = RequestMethod.POST)
    public ModelAndView registerFormPost(@ModelAttribute("user") User user) throws SQLException {
        ModelAndView mav = new ModelAndView();

        AnswerResponse answerResponse = userService.registerUser(user);
        if ( !answerResponse.isError() ) {
            //регистрация успешна
            mav.setViewName("register/register-ok");
        } else {
            //ошибка
            mav.addObject("error", "Ошибка при регистрации. " + answerResponse.getErrorMsg());
            mav.addObject("user", user);
            mav.setViewName("register/index");
        }

        return mav;
    }


}
