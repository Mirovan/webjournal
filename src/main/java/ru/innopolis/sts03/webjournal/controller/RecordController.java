package ru.innopolis.sts03.webjournal.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import ru.innopolis.sts03.webjournal.hibernate.entity.RecordEntity;
import ru.innopolis.sts03.webjournal.hibernate.entity.UserEntity;
import ru.innopolis.sts03.webjournal.model.User;
import ru.innopolis.sts03.webjournal.servicelayer.exception.ExceptionService;
import ru.innopolis.sts03.webjournal.servicelayer.service.RecordService;
import ru.innopolis.sts03.webjournal.servicelayer.service.UserService;
import ru.innopolis.sts03.webjournal.model.Record;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.SQLException;

/**
 * Контроллер обработки записей
 */
@Controller
public class RecordController {


    private static final Logger LOG = LoggerFactory.getLogger(RecordController.class);

    @Autowired
    private UserService userService;

    @Autowired
    private RecordService recordService;


    /**
     * отображение формы добавления записи
     * */
    @RequestMapping(value = "/profile/addrecord/", method = RequestMethod.GET)
    public ModelAndView addRecordForm(HttpServletRequest req, HttpServletResponse resp) {
        ModelAndView mav = new ModelAndView();

        mav.addObject("action", "/profile/addrecord/");
        mav.setViewName("profile/addrecord");

        return mav;
    }


    /**
     * Обработка post запроса добавление записи
     * */
    @RequestMapping(value = "/profile/addrecord/", method = RequestMethod.POST)
    public ModelAndView addRecordFormPost(HttpServletRequest req, @ModelAttribute("record") Record record) throws SQLException {
        ModelAndView mav = new ModelAndView();

        String login = userService.getPrincipal();
        User user = userService.getUserByLogin( login );

        if ( recordService.addRecord(login, record.getTitle(), record.getContent()) ) {
            return new ModelAndView("redirect:/profile/");
        } else {
            mav.addObject("error", "Ошибка добавления");
            mav.setViewName("/profile/addrecord");
        }

        return mav;
    }


    /**
     * Отображение записи
     * */
    @RequestMapping(value = "/blog/showrecord/{recordId}", method = RequestMethod.GET)
    public ModelAndView showRecord(@PathVariable int recordId) throws ExceptionService {
        ModelAndView mav = new ModelAndView();

        Record record = recordService.getElementById(recordId);
        User user = userService.getUserById(record.getId());
        mav.addObject("user", user);
        mav.addObject("record", record);
        mav.setViewName("/blog/showrecord");

        return mav;
    }


    /**
     * Форма редактирования
     * */
    @RequestMapping(value = "/profile/editrecord/{recordId}", method = RequestMethod.GET)
    public ModelAndView editRecordForm(@PathVariable int recordId) throws ExceptionService {
        ModelAndView mav = new ModelAndView();

        Record record = recordService.getElementById(recordId);
        User user = userService.getUserById(record.getId());
        mav.addObject("user", user);
        mav.addObject("record", record);
        mav.addObject("action", "/profile/editrecord/"+recordId);
        mav.setViewName("/profile/addrecord");

        return mav;
    }



    /**
     * обработка запроса редактироваия записи
     * */
    @RequestMapping(value = "/profile/editrecord/{recordId}", method = RequestMethod.POST)
    public ModelAndView editRecordFormPost(@ModelAttribute("record") Record record, @PathVariable int recordId) throws ExceptionService {
        ModelAndView mav = new ModelAndView();

        record.setId(recordId);
        record.setUserid( userService.getUserByLogin(userService.getPrincipal()).getId() );
        record.setCreatedate( recordService.getElementById(recordId).getCreatedate() );
        if ( recordService.editRecord(record) ) {
            return new ModelAndView("redirect:/profile/");
        } else {
            mav.addObject("error", "Ошибка добавления");
            mav.setViewName("/profile/addrecord");
        }

        return mav;
    }


    /**
     * удаление записи
     * */
    @RequestMapping(value = "/profile/delrecord/{recordId}", method = RequestMethod.GET)
    public ModelAndView delRecord(@PathVariable int recordId) throws ExceptionService {
        ModelAndView mav = new ModelAndView();

        if ( recordService.deleteRecord(recordId) ) {
            return new ModelAndView("redirect:/profile/");
        } else {
            mav.addObject("error", "Ошибка Удаления");
            mav.setViewName("/profile/error");
        }

        return mav;
    }


}
