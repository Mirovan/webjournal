package ru.innopolis.sts03.webjournal.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import ru.innopolis.sts03.webjournal.hibernate.entity.RecordEntity;
import ru.innopolis.sts03.webjournal.hibernate.entity.UserEntity;
import ru.innopolis.sts03.webjournal.model.Record;
import ru.innopolis.sts03.webjournal.model.User;
import ru.innopolis.sts03.webjournal.servicelayer.exception.ExceptionService;
import ru.innopolis.sts03.webjournal.servicelayer.service.RecordService;
import ru.innopolis.sts03.webjournal.servicelayer.service.UserService;

import javax.servlet.http.*;
import java.sql.SQLException;
import java.util.List;

/**
 * Контроллер - страница пользователя
 */
@Controller
public class UserController {

    private static final Logger LOG = LoggerFactory.getLogger(UserController.class);

    @Autowired
    private UserService userService;

    @Autowired
    private RecordService recordService;

    /**
     * Главная страница кабинета пользователя
     * */
    @RequestMapping(value = "/profile/", method = RequestMethod.GET)
    public ModelAndView userPage(HttpServletRequest req, HttpServletResponse resp) throws SQLException {
        ModelAndView mav = new ModelAndView();

        String login = userService.getPrincipal();
            if ( !login.equals("anonymousUser") ) {
            User user = userService.getUserByLogin(login);

            List<Record> data = recordService.getAllByUser(user);
            mav.addObject("data", data);
            mav.setViewName("profile/index");
        } else {
            return new ModelAndView("redirect:/auth/");
        }
        return mav;
    }


    /**
     *показать всех пользователей
     * */
    @RequestMapping(value = "/blogs/", method = RequestMethod.GET)
    public ModelAndView showAllUsers() throws SQLException {
        ModelAndView mav = new ModelAndView();

        List<User> data = userService.getAll();
        mav.addObject("data", data);
        mav.setViewName("blog/index");

        return mav;
    }


    /**
     * показывает записи пользователя
     * */
    @RequestMapping(value = "/blog/{userId}", method = RequestMethod.GET)
    public ModelAndView showUserBlog(@PathVariable int userId) throws ExceptionService {
        ModelAndView mav = new ModelAndView();

        User user = userService.getUserById(userId);
        mav.addObject("user", user);

        List<Record> data = recordService.getAllByUser(user);
        mav.addObject("data", data);
        mav.setViewName("blog/recordlist");

        return mav;
    }


}
