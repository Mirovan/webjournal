<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<c:import url="/WEB-INF/view/header.jsp"/>


<div class="blog-header">
    <h1 class="blog-title">Auth</h1>
</div>

<div class="row">

    <h1>Hello, ${user.login} [${user.name} ${user.sirname}]</h1>

</div><!-- /.row -->

<c:import url="/WEB-INF/view/footer.jsp"/>