<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<c:import url="/WEB-INF/view/header.jsp"/>


<div class="blog-header">
    <h1 class="blog-title">Users</h1>
</div>


<div class="row">

    <div style="margin: 0 0 20px 0;">
        <c:forEach var="user" items="${data}">
            <div style="display: block;">
                <a href="/blog/${user.id}">
                        ${user.name} ${user.sirname}
                </a>
            </div>
        </c:forEach>
    </div>

</div><!-- /.row -->

<c:import url="/WEB-INF/view/footer.jsp"/>