<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<c:import url="/WEB-INF/view/header.jsp"/>

<div class="blog-header">
    <h1 class="blog-title">Запись в блоге</h1>
</div>

<div class="row">

    <h2><c:out value="${record.title}"/></h2>
    <strong><c:out value="${user.login}"/></strong> [<fmt:formatDate value="${record.createdate}" pattern="dd-MM-yyyy HH:mm" />]
    <p>
        <c:out value="${record.content}" escapeXml="true"/>
    </p>

</div><!-- /.row -->


<c:import url="/WEB-INF/view/footer.jsp"/>