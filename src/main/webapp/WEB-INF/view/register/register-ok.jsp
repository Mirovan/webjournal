<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<c:import url="/WEB-INF/view/header.jsp"/>


<div class="blog-header">
    <h1 class="blog-title">Register</h1>
</div>

<div class="row">

    <h2>Register OK!</h2>
    <a href="/auth/">Please, login</a>

</div><!-- /.row -->

<c:import url="/WEB-INF/view/footer.jsp"/>