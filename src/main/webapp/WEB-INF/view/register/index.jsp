<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<c:import url="/WEB-INF/view/header.jsp"/>


<div class="blog-header">
    <h1 class="blog-title">Register</h1>
</div>

<div class="row">

    <c:if test="${error ne null}">
        <div style="background: #FF0000; padding: 10px;">
            <c:out value="${error}" />
        </div>
    </c:if>

    <form:form method="post" action="/register/">
        <div class="form-group">
            <label for="loginInput">Login</label>
            <input type="text" name="login" class="form-control" id="loginInput" placeholder="Login" value="${user.login}">
        </div>
        <div class="form-group">
            <label for="passInput">Password</label>
            <input type="password" name="password" class="form-control" id="passInput" placeholder="Password">
        </div>
        <div class="form-group">
            <label for="emailInput">Email address</label>
            <input type="email" name="email" class="form-control" id="emailInput" placeholder="Email" value="${user.email}">
        </div>
        <div class="form-group">
            <label for="nameInput">Name</label>
            <input type="text" name="name" class="form-control" id="nameInput" value="${user.name}">
        </div>
        <div class="form-group">
            <label for="sirnameInput">SirName</label>
            <input type="text" name="sirname" class="form-control" id="sirnameInput" value="${user.sirname}">
        </div>
        <div class="form-group">
            <input type="submit" class="btn btn-primary">
        </div>

        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
    </form:form>

</div><!-- /.row -->

<c:import url="/WEB-INF/view/footer.jsp"/>