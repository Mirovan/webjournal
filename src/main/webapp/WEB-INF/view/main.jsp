<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<c:import url="/WEB-INF/view/header.jsp"/>

<div class="blog-header">
    <h1 class="blog-title">Запись в журнале</h1>
</div>


<div class="row">
    <h2>Last posts</h2>
        <c:forEach var="rec" items="${data}">
            <div style="display: block;">
                <a href="/blog/showrecord/${rec.id}">
                    ${rec.title}
                </a> [<fmt:formatDate value="${rec.createdate}" pattern="dd-MM-yyyy HH:mm" />]
            </div>
        </c:forEach>
</div><!-- /.row -->

<c:import url="/WEB-INF/view/footer.jsp"/>