<%@ page import="ru.innopolis.sts03.webjournal.PageAccess" %>
<%
    if ( !PageAccess.isUserAuth(request) ) response.sendRedirect("/auth/index.jsp");
%>
<%@include file="../header.jsp" %>


<div class="blog-header">
    <h1 class="blog-title">Add post</h1>
</div>

<div class="row">

    <form method="post" action="addrecord.jsp">
        <div class="form-group">
            <label for="loginInput">Login</label>
            <input type="text" name="login" class="form-control" id="loginInput" placeholder="Login">
        </div>
        <div class="form-group">
            <label for="passInput">Password</label>
            <input type="password" name="password" class="form-control" id="passInput" placeholder="Password">
        </div>
        <div class="form-group">
            <input type="submit" class="btn btn-primary">
        </div>
    </form>

</div><!-- /.row -->

<%@include file="../footer.jsp" %>