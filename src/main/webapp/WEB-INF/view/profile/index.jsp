<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<c:import url="/WEB-INF/view/header.jsp"/>


<div class="blog-header">
    <h1 class="blog-title">User cabinet</h1>
</div>

<div class="row">

    <a href="/profile/addrecord/">Новая запись</a>
    <hr />

    <h3>Мои записи:</h3>
    <div style="margin: 0 0 20px 0;">
        <c:forEach var="rec" items="${data}">
            <div style="display: block; background: #cccccc; margin: 5px; padding: 1px; width: 500px;">
                <div style="float: left; width: 300px; background: #9acfea; padding: 0 5px;">
                    <a href="/blog/showrecord/${rec.id}">
                            ${rec.title}
                    </a> [<fmt:formatDate value="${rec.createdate}" pattern="dd-MM-yyyy HH:mm" />]
                    </div>
                <div style="float: left; width: 50px; background: #ffff00;">
                    <a href="/profile/editrecord/${rec.id}">Edit</a>
                </div>
                <div style="float: left; width: 50px; background: #ff0000;">
                    <a href="/profile/delrecord/${rec.id}">Del</a>
                </div>

                <div style="clear: both;"></div>
            </div>
        </c:forEach>
    </div>

</div>


</div><!-- /.row -->

<c:import url="/WEB-INF/view/footer.jsp"/>