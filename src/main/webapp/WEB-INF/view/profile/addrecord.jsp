<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<c:import url="/WEB-INF/view/header.jsp"/>


<div class="blog-header">
    <h1 class="blog-title">Add post</h1>
</div>

<div class="row">

    <c:if test="${error ne null}">
        <div style="background: #FF0000; padding: 10px;">
            <c:out value="${error}" />
        </div>
    </c:if>

    <form:form method="post" action="${action}">
        <div class="form-group">
            <label for="titleInput">Title</label>
            <input type="text" name="title" class="form-control" id="titleInput" value="${record.title}">
        </div>
        <div class="form-group">
            <textarea name="content" rows="10" cols="70"><c:out value="${record.content}" /></textarea>
        </div>
        <div class="form-group">
            <input type="submit" class="btn btn-primary" name="submit">
        </div>

        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
    </form:form>

</div><!-- /.row -->

<c:import url="/WEB-INF/view/footer.jsp"/>