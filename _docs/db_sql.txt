CREATE SEQUENCE user_id_seq START WITH 2;

CREATE TABLE "users" (
    id INTEGER NOT NULL default nextval('user_id_seq') PRIMARY KEY,
	login VARCHAR(120) NOT NULL,
	password VARCHAR(120) NOT NULL,
	enabled INTEGER NOT NULL DEFAULT 1,
	roleid INTEGER NOT NULL,
    name VARCHAR(120) NOT NULL,
    sirname VARCHAR(120) NOT NULL,
	email VARCHAR(120) NOT NULL
);

ALTER SEQUENCE user_id_seq owned by "users".id;


INSERT INTO "users" VALUES(1, 'admin', '000000', 1, 1, 'Admin', 'Админ', 'a@a.ru');



CREATE SEQUENCE role_id_seq START WITH 3;

CREATE TABLE role (
    id INTEGER NOT NULL default nextval('role_id_seq') PRIMARY KEY,
	name VARCHAR(120) NOT NULL
);

ALTER SEQUENCE role_id_seq owned by role.id;

INSERT INTO role VALUES(1, 'ROLE_ADMIN');
INSERT INTO role VALUES(2, 'ROLE_USER');



CREATE SEQUENCE record_id_seq;

CREATE TABLE records (
    id INTEGER NOT NULL default nextval('record_id_seq') PRIMARY KEY,
	userid INTEGER NOT NULL,
	title VARCHAR(120) NOT NULL,
	"content" TEXT,
	createdate TIMESTAMP	
);

ALTER SEQUENCE record_id_seq owned by records.id;







